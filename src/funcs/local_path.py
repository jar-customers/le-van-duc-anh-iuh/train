import os
import inspect
from config import local_env


def realpath(*p:str)->str:
    return os.path.realpath(os.path.join(*p))


def absolute(*p:str)->str:
    if p[0].startswith(('./', '../', '.\\', '..\\')):
        return realpath(os.path.dirname(inspect.stack()[1][1]), *p)
    return realpath(*p)


def from_current_file(file_path:str, *p:str)->str:
    return realpath(os.path.dirname(file_path), *p)


def from_root_dir(*p:str)->str:
    return realpath(local_env.ROOT_DIR, *p)

def from_cd_dir(*p:str)->str:
    return realpath(local_env.CW_DIR, *p)


def make_sure_dir_exists(dir_path:str):
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)


def list_all_files(absolute_dir:str):
    raw_files = os.listdir(absolute_dir)
    raw_files = [os.path.join(absolute_dir, file_name) for file_name in raw_files]
    files = list(filter(lambda v: os.path.isfile(v), raw_files))
    return files

