import os
import numpy as np
import cv2
from uuid import uuid4
from funcs import local_path
import random

def make_binary_label(labels:list, unique_labelnames:list, dtype='float32'):
    binary_label_shape = (len(unique_labelnames))
    labelnames_processed = np.array(unique_labelnames)
    labelnames_processed.sort()

    dict_labels = {}

    for i, item in enumerate(labelnames_processed):
        dict_labels[item] = int(i)
    
    labels_processed = []
    for label in labels:
        index = dict_labels[label]
        binary_label = np.zeros(binary_label_shape, dtype=dtype)
        binary_label[index] = 1.
        labels_processed.append(binary_label)

    labels_processed = np.array(labels_processed, dtype='float32')
    return labels_processed, labelnames_processed


def trim_data(x:np.ndarray, y:np.ndarray):
    if x.shape[0] != y.shape[0]:
        print('ERROR: Dataset length of x must be equal to length of y')
        return x, y

    original_size = x.shape[0]
    max_size = int(original_size/8)*8

    if max_size == original_size:
        return x, y
        
    return x[:max_size], y[:max_size]


def load_dataset(absolute_data_dir, prev_processing_function=None, normalize_label:bool=True, absolute_cache_dir:str=None, package_size:int=1024):
    if absolute_cache_dir == None:
        raise('absolute_cache_dir is required')
    
    # os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    # init
    labels_mames = []
    labels_imagepaths = []
    print(f'\n[i] Đang liệt kê danh sách các files\n')
    for dir_path, dirnames, filenames in os.walk(absolute_data_dir):
        #! if folder was empty
        if len(filenames) == 0:
            continue
        
        label_name = dir_path.replace(absolute_data_dir, '').replace('\\', '/').strip('/')
        labels_mames.append(label_name)

        dir_path_print = '...' + dir_path[:70] if len(dir_path) > 80 else dir_path
        print(f'    + Label: "{label_name}" : "{dir_path_print}"')
        for i, filename in enumerate(filenames):
            absolute_file_path = os.path.realpath(os.path.join(dir_path, filename))
            labels_imagepaths.append([label_name, absolute_file_path])

        #for
    #for

    print('Random....')
    random.shuffle(labels_imagepaths)
    print(labels_imagepaths[0:5])

    length = len(labels_imagepaths)
    number_of_packages = int(length/package_size)
    number_of_packages_plus = (number_of_packages + 1) if (number_of_packages*package_size < length) else number_of_packages
    final_datas:list[str] = []
    final_labelmames:np.ndarray = None


    for i in range(number_of_packages_plus):
        start_index = i * package_size
        end_index = (start_index + package_size) if i < number_of_packages else length
        images = []
        labels = []
        part_of_labels_imagepaths = labels_imagepaths[start_index:end_index]

        for label, image_path in part_of_labels_imagepaths:
            img = cv2.imread(image_path)
            if prev_processing_function != None:
                img = prev_processing_function(img)
            
            images.append(img)
            labels.append(label)
            

        final_images = np.array(images, dtype="float32")
        final_labels, final_labelmames = make_binary_label(labels, labels_mames)
        
        # save to cache
        _id = str(uuid4())
        with open(os.path.join(absolute_cache_dir, _id + '.npy'), 'wb') as f:
            np.save(f, final_images)
            np.save(f, final_labels)

        final_datas.append(_id)
        print(f'Package[{i}/{number_of_packages_plus-1}]: {final_images.shape[0]} {i*package_size + final_images.shape[0]}/{length}')
    


    print_labels = '; '.join(final_labelmames)
    print(f'[=>] Tổng số image đã load và xử lý: {length} image')
    print(f'[=>] Labels: {print_labels}')
    return final_datas, final_labelmames


def load_from_package(_id:str, absolute_cache_dir:str=None):
    with open(os.path.join(absolute_cache_dir, _id + '.npy'), 'rb') as f:
        final_images:np.ndarray = np.load(f)
        final_labels:np.ndarray = np.load(f)
    return final_images, final_labels